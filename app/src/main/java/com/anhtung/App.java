package com.anhtung;

import android.content.Context;
import android.content.res.Configuration;

import androidx.annotation.NonNull;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import androidx.room.Room;

import com.anhtung.youtubemusic.data.local.SuggestionDB;
import com.anhtung.youtubemusic.utils.LocaleUtils;
import com.anhtung.youtubemusic.utils.MyDebugTree;
import com.google.firebase.BuildConfig;

import dagger.hilt.android.HiltAndroidApp;
import io.reactivex.rxjava3.plugins.RxJavaPlugins;
import timber.log.Timber;

@HiltAndroidApp
public class App extends MultiDexApplication {
    private static App instance;
    private SuggestionDB suggestionDB;

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        RxJavaPlugins.setErrorHandler(Timber::w);
        initLog();
        initConfig();

    }

    public SuggestionDB getSuggestionDB() {
        return suggestionDB;
    }

    private void initConfig() {
        suggestionDB = Room.databaseBuilder(this,
                SuggestionDB.class,"suggestion-db").build();
    }

    private void initLog() {
        if(BuildConfig.DEBUG){
            Timber.plant(new MyDebugTree());
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleUtils.applyLocale(this);
    }
}
