package com.anhtung.youtubemusic.utils;

import androidx.annotation.Nullable;

import timber.log.Timber;

public class MyDebugTree extends Timber.DebugTree {
    @Nullable
    @Override
    protected String createStackElementTag(StackTraceElement element) {
        return String.format("(%s:%s)#%s",
                element.getFileName(),
                element.getLineNumber(),
                element.getMethodName());
    }
}
