package com.anhtung.youtubemusic.common;

public class Constant {
    public static  final String BASE_URL ="https://youtube.googleapis.com/youtube/v3/";

    public static final String API_KEY = "AIzaSyDjLqIx8zhk7D3_M_sObmd_H_hWagr-oS4";

    //key đang dùng AIzaSyB1CVK44TiCmavtcFDuCxZ5HUCn3DGZPbU
    //key khác AIzaSyDjLqIx8zhk7D3_M_sObmd_H_hWagr-oS4

    public static  final int CONNECT_S =30;
    public static  final int READ_S =30;
    public static  final int WRITE_S =30;

    public static  final String  LANGUAGE_EN ="en";
    public static  final String  LANGUAGE_VN ="vi";


    /*SharePreference constant*/
    public static final String PREF_SETTING_LANGUAGE="pref_setting_language";


    /*Query Param*/
    public static final int MAX_RESULT = 100;
    public static final String REGION_CODE = "VN";
    public static final String VIDEO = "VIDEO";

    /*Event Bus*/
    public static final int PLAY_VIDEO = 5;
    public static final int PAUSE_VIDEO = 6;

    /*Setting pager fragment*/
    public static final int TYPE_HOME = 0;
    public static final String ON_PLAY_BACKGROUND = "ON_PLAY_BACKGROUND";
    public static final String OFF_PLAY_BACKGROUND = "OFF_PLAY_BACKGROUND";
    public static final String LINK_GOOGLE = "https://support.google.com/youtube/answer/9288567?hl=vi";
    public static final String CHANNEL_ID = "CHANNEL_ID";
    public static final String PRIORITY_CHANNEL_ID = "PRIORITY_CHANNEL_ID";
    public static final int REQUEST_CODE_1 = 1;
    public static final int NOTIFICATION_ID_1 = 1;

    /*Top fragment*/
    public static final int NEXT_VIDEO = 3;
    public static final int ENTER_FULL_SCREEN = 50;
    public static final int EXIT_FULL_SCREEN = 51;
    public static final int SHOW_NAVIGATION = 52;
    public static final int HIDE_NAVIGATION = 53;
    public static final int PREVIOUS_VIDEO = 54;
    public static final int TYPE_VIDEO = 55;
    public static final int TYPE_CHANNEL = 56;
    public static final int TYPE_IMG = 57;
    public static final int LEVEL_PLAY = 58;
    public static final int SEEK_BAR = 61;
    public static final int TYPE_TITLE = 62;
    public static final int TYPE_TITLE_CHANNEL = 63;
    public static final int HIDE_NAVIGATION_TO_WEB_VIEW = 64;
}
