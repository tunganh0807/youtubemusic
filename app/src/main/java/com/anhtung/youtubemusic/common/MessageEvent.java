package com.anhtung.youtubemusic.common;

import com.anhtung.youtubemusic.data.model.video.Item;

import java.util.List;

public class MessageEvent {
    private int typeEvent = 0;
    private String stringValue = "";
    private Item item;
    private List<Item> listItem;
    private boolean isBoolean;

    public MessageEvent(Item item) {
        this.item = item;
    }

    public MessageEvent(int typeEvent, String stringValue) {
        this.typeEvent = typeEvent;
        this.stringValue = stringValue;
    }

    public MessageEvent(int typeEvent) {
        this.typeEvent = typeEvent;
    }

    public MessageEvent(List<Item> listItem) {
        this.listItem = listItem;
    }

    public int getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(int typeEvent) {
        this.typeEvent = typeEvent;
    }

    public String getStringValue() {
        return stringValue;
    }

    public Item getItem() {
        return item;
    }

    public List<Item> getListItem() {
        return listItem;
    }

    public boolean isBoolean() {
        return isBoolean;
    }

    public void setBoolean(boolean aBoolean) {
        isBoolean = aBoolean;
    }
}
