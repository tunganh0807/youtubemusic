package com.anhtung.youtubemusic.common;

public class Event {
    int typeEvent;
    String newNameFile;
    String newPathFile;

    public Event(int typeEvent, String newNameFile, String newPathFile) {
        this.typeEvent = typeEvent;
        this.newNameFile = newNameFile;
        this.newPathFile = newPathFile;
    }
}
