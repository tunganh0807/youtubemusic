package com.anhtung.youtubemusic.data.model.channel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Channel {
    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("etag")
    @Expose
    private String etag;
    @SerializedName("pageInfo")
    @Expose
    private PageInfoChannel pageInfo;
    @SerializedName("items")
    @Expose
    private List<ItemChannel> items = null;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public PageInfoChannel getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfoChannel pageInfo) {
        this.pageInfo = pageInfo;
    }

    public List<ItemChannel> getItems() {
        return items;
    }

    public void setItems(List<ItemChannel> items) {
        this.items = items;
    }
}
