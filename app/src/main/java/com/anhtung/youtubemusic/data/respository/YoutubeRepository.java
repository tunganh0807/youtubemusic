package com.anhtung.youtubemusic.data.respository;

import com.anhtung.youtubemusic.common.Constant;
import com.anhtung.youtubemusic.data.model.channel.Channel;
import com.anhtung.youtubemusic.data.model.comment.Comment;
import com.anhtung.youtubemusic.data.model.item_video.ItemVideo;
import com.anhtung.youtubemusic.data.model.item_video.ItemVideoItem;
import com.anhtung.youtubemusic.data.model.search.ItemSearch;
import com.anhtung.youtubemusic.data.model.search.Search;
import com.anhtung.youtubemusic.data.model.video.Item;
import com.anhtung.youtubemusic.data.model.video.Video;
import com.anhtung.youtubemusic.data.model.video.VideoItem;
import com.anhtung.youtubemusic.data.remote.YoutubeAPI;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.rxjava3.core.Single;

public class YoutubeRepository {
    private final YoutubeAPI youtubeAPI;

    @Inject
    public YoutubeRepository(YoutubeAPI youtubeAPI) {
        this.youtubeAPI = youtubeAPI;
    }

    /*Make to trending fragment*/
    public Single<Video> getVideo(int maxResult, String key, String region) {
        return youtubeAPI.getVideo(maxResult, key, region).flatMap(video -> {
            List<Channel> channelList = new ArrayList<>(); // tạo 1 danh sách channel
            List<Item> list = video.getItems(); // Lấy được danh sách Item từ Video

            for (int i = 0; i < list.size(); i++) {
                channelList.add(new Channel());
            }

            return Single.zip(getChannelItem(video, channelList), objects -> {
                for (int i = 0; i < list.size(); i++) {
                    Item item = list.get(i);
                    item.setChannel(channelList.get(i));
                }
                return video;
            });
        });
    }

    public Single<Channel> getChannel(String id, String key, List<Channel> list, int pos) {
        return youtubeAPI.getChannel(id, key).flatMap(channel -> {
            list.set(pos, channel);
            return Single.just(channel);
        });
    }

    private List<Single<Channel>> getChannelItem(Video video, List<Channel> channelList) {
        List<Single<Channel>> list = new ArrayList<>();
        List<Item> listItem = video.getItems();
        for (int i = 0; i < listItem.size(); i++) {
            Item item = listItem.get(i);
            list.add(getChannel(item.getSnippet().getChannelId(), Constant.API_KEY, channelList, i));
        }
        return list;
    }


    /*Make to DragView in trending fragment*/
    public Single<ItemVideo> getItemVideo(String id, String key) {
        // Trả về 1 itemVideo
        return youtubeAPI.getItemVideo(id, key).flatMap(itemVideo -> {
            // Lấy được danh sách listItemReal từ itemVideo
            List<ItemVideoItem> listItemReal = itemVideo.getItems();
            List<Channel> channelList = new ArrayList<>();

            for (int i = 0; i < listItemReal.size(); i++) {
                channelList.add(new Channel());
            }

            return Single.zip(getChannelItemVer2(itemVideo, channelList), objects -> {
                for (int i = 0; i < listItemReal.size(); i++) {
                    ItemVideoItem item = listItemReal.get(i);
                    item.setChannel(channelList.get(i));
                }
                return itemVideo;
            });
        });
    }

    private List<Single<Channel>> getChannelItemVer2(ItemVideo itemVideo, List<Channel> channelList) {
        List<Single<Channel>> list = new ArrayList<>();
        List<ItemVideoItem> listItemReal = itemVideo.getItems();

        for (int i = 0; i < listItemReal.size(); i++) {
            ItemVideoItem item = listItemReal.get(i);
            list.add(getChannel(item.getSnippet().getChannelId(), Constant.API_KEY, channelList, i));
        }
        return list;
    }


    /*Make Comment fragment*/
    public Single<ItemVideo> getItemVideo3(String id, String key) {
        return youtubeAPI.getItemVideo(id, key).flatMap(itemVideo -> {
            List<ItemVideoItem> listItemReal = itemVideo.getItems();
            List<Comment> commentList = new ArrayList<>();

            for (int i = 0; i < listItemReal.size(); i++) {
                commentList.add(new Comment());
            }

            return Single.zip(getCommentItem(itemVideo, commentList), objects -> {
                for (int i = 0; i < listItemReal.size(); i++) {
                    ItemVideoItem item = listItemReal.get(i);
                    item.setComment(commentList.get(i));
                }
                return itemVideo;
            });
        });
    }

    private List<Single<Comment>> getCommentItem(ItemVideo itemVideo, List<Comment> commentList) {
        List<Single<Comment>> list = new ArrayList<>();
        List<ItemVideoItem> listItemReal = itemVideo.getItems();

        for (int i = 0; i < listItemReal.size(); i++) {
            ItemVideoItem item = listItemReal.get(i);
            list.add(getComment(Constant.MAX_RESULT, item.getId(), Constant.API_KEY, commentList, i));
        }
        return list;
    }

    public Single<Comment> getComment(int maxResults, String videoId, String key, List<Comment> commentList, int pos) {
        return youtubeAPI.getCommentYoutube(maxResults, videoId, key).flatMap(comment -> {
            commentList.set(pos, comment);
            return Single.just(comment);
        });
    }


    /*Make search fragment*/
    public Single<Search> getVideoSearch(int maxResults, String q, String type, String key) {
        return youtubeAPI.getVideoSearch(maxResults, q, type, key);
    }


    /*Make handler video search*/
    public Single<Search> getHandlerVideoSearch(int maxResults, String q, String type, String key) {
        return youtubeAPI.getVideoSearch(maxResults, q, type, key).flatMap(search -> {
            List<ItemSearch> listSearch = search.getItems();
            List<VideoItem> videoList = new ArrayList<>();

            for (int i = 0; i < listSearch.size(); i++) {
                videoList.add(new VideoItem());
            }

            return Single.zip(getVideoItem(search, videoList), objects -> {
                for (int i = 0; i < listSearch.size(); i++) {
                    ItemSearch item = listSearch.get(i);
                    item.setVideoItem(videoList.get(i));
                }
                return search;
            });
        });
    }

    private List<Single<VideoItem>> getVideoItem(Search search, List<VideoItem> videoList) {
        List<Single<VideoItem>> list = new ArrayList<>();
        List<ItemSearch> listItem = search.getItems();
        for (int i = 0; i < listItem.size(); i++) {
            ItemSearch itemSearch = listItem.get(i);
            list.add(getItemVideo4(itemSearch.getId().getVideoId(), Constant.API_KEY, videoList, i));
        }
        return list;
    }

    private Single<VideoItem> getItemVideo4(String videoId, String apiKey, List<VideoItem> videoList, int pos) {
        return youtubeAPI.getVideoItem(videoId, apiKey).flatMap(itemVideo -> {
            videoList.set(pos, itemVideo);
            return Single.just(itemVideo);
        });
    }

    /*Fix bug search api*/
    public Single<Search> fixSearchApi(int maxResults, String q, String type, String key) {
        return youtubeAPI.getVideoSearch(maxResults, q, type, key).flatMap(search -> {
            List<ItemSearch> listSearch = search.getItems();
            List<VideoItem> videoList = new ArrayList<>();

            for (int i = 0; i < listSearch.size(); i++) {
                videoList.add(new VideoItem());
            }

            return Single.zip(getVideoItemFix(search, videoList), objects -> {
                for (int i = 0; i < listSearch.size(); i++) {
                    ItemSearch item = listSearch.get(i);
                    item.setVideoItem(videoList.get(i));
                }
                return search;
            });
        });
    }

    private List<Single<VideoItem>> getVideoItemFix(Search search, List<VideoItem> videoList) {
        List<Single<VideoItem>> list = new ArrayList<>();
        List<ItemSearch> listItem = search.getItems();
        for (int i = 0; i < listItem.size(); i++) {
            ItemSearch itemSearch = listItem.get(i);
            list.add(getItemVideoFix(itemSearch.getId().getVideoId(), Constant.API_KEY, videoList, i));
        }
        return list;
    }

    private Single<VideoItem> getItemVideoFix(String videoId, String apiKey, List<VideoItem> videoList, int pos) {
        return youtubeAPI.getVideoItem(videoId, apiKey).flatMap(itemVideo -> {
            videoList.set(pos, itemVideo);

            List<Item> itemList = itemVideo.getItems();
            List<Channel> channelList = new ArrayList<>();

            for (int i = 0; i < itemList.size(); i++) {
                channelList.add(new Channel());
            }

            return Single.zip(getChannelItemFix(itemVideo, channelList), objects -> {
                for (int i = 0; i < itemList.size(); i++) {
                    Item item = itemList.get(i);
                    item.setChannel(channelList.get(i));
                }
                return itemVideo;
            });
        });
    }

    private List<Single<Channel>> getChannelItemFix(VideoItem itemVideo, List<Channel> channelList) {
        List<Single<Channel>> list = new ArrayList<>();
        List<Item> itemList = itemVideo.getItems();
        for (int i = 0; i < itemList.size(); i++) {
            Item item = itemList.get(i);
            list.add(getChannelFix(item.getSnippet().getChannelId(), Constant.API_KEY, channelList, i));
        }
        return list;
    }

    private Single<Channel> getChannelFix(String channelId, String apiKey, List<Channel> channelList, int pos) {
        return youtubeAPI.getChannel(channelId, apiKey).flatMap(channel -> {
            channelList.set(pos, channel);
            return Single.just(channel);
        });
    }
}
