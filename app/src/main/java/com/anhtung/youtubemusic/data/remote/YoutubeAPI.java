package com.anhtung.youtubemusic.data.remote;

import com.anhtung.youtubemusic.data.model.channel.Channel;
import com.anhtung.youtubemusic.data.model.comment.Comment;
import com.anhtung.youtubemusic.data.model.item_video.ItemVideo;
import com.anhtung.youtubemusic.data.model.search.Search;
import com.anhtung.youtubemusic.data.model.video.Video;
import com.anhtung.youtubemusic.data.model.video.VideoItem;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface YoutubeAPI {
    @GET("videos?part=snippet,contentDetails,statistics&chart=mostPopular")
    Single<Video> getVideo(@Query("maxResults") int maxResults,
                           @Query("key") String key,
                           @Query("regionCode") String regionCode);

    @GET("channels?part=snippet&part=statistics")
    Single<Channel> getChannel(@Query("id") String id, @Query("key") String key);

    @GET("videos?part=snippet,contentDetails,statistics")
    Single<ItemVideo> getItemVideo(@Query("id") String id, @Query("key") String key);

    @GET("commentThreads?part=snippet")
    Single<Comment> getCommentYoutube(@Query("maxResults") int maxResults,
                                      @Query("videoId") String videoId,
                                      @Query("key") String key);

    @GET("search?part=snippet")
    Single<Search> getVideoSearch(@Query("maxResults") int maxResults,
                                  @Query("q") String q,
                                  @Query("type") String type,
                                  @Query("key") String key);

    @GET("videos?part=snippet,contentDetails,statistics")
    Single<VideoItem> getVideoItem(@Query("id") String id, @Query("key") String key);
}
