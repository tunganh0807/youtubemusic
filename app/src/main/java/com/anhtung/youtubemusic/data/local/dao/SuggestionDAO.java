package com.anhtung.youtubemusic.data.local.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.anhtung.youtubemusic.data.local.entities.Suggestion;

import java.util.List;

@Dao
public interface SuggestionDAO {

    @Query("SELECT * FROM suggestion")
    List<Suggestion> getSuggestions();

    @Insert
    void insertSuggestion(Suggestion... suggestions);

}
