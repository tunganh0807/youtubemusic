package com.anhtung.youtubemusic.data.respository;

import com.anhtung.App;
import com.anhtung.youtubemusic.data.local.entities.Suggestion;

import java.util.ArrayList;
import java.util.List;

public class SuggestionRepository {
    private static SuggestionRepository instance;

    public SuggestionRepository() {
        // for singleton
    }

    public static SuggestionRepository getInstance() {
        if (instance == null) {
            instance = new SuggestionRepository();
        }
        return instance;
    }

    public void addSuggestion(Suggestion... suggestion) {
        new Thread() {
            @Override
            public void run() {
                try {
                    App.getInstance().getSuggestionDB().getSuggestionDAO().insertSuggestion(suggestion);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }


    public void getListSuggestion(OnResultCallBack cb) {
        new Thread() {
            public void run() {
                try {
                    List<Suggestion> list = App.getInstance().getSuggestionDB()
                            .getSuggestionDAO().getSuggestions();
                    cb.callBack(list);
                } catch (Exception e) {
                    cb.callBack(new ArrayList<>());
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public interface OnResultCallBack {
        void callBack(Object data);
    }
}
