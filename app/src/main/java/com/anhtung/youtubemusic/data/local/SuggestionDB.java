package com.anhtung.youtubemusic.data.local;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.anhtung.youtubemusic.data.local.dao.SuggestionDAO;
import com.anhtung.youtubemusic.data.local.entities.Suggestion;

@Database(version = 1, entities = {Suggestion.class})
public abstract class SuggestionDB extends RoomDatabase {
    public abstract SuggestionDAO getSuggestionDAO();
}
