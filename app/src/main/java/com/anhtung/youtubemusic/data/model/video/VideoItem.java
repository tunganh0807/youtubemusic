package com.anhtung.youtubemusic.data.model.video;

import com.anhtung.youtubemusic.data.model.item_video.PageInfoVideo;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VideoItem {
    @SerializedName("kind")
    @Expose
    private String kind;
    @SerializedName("etag")
    @Expose
    private String etag;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;
    @SerializedName("pageInfo")
    @Expose
    private PageInfoVideo pageInfo;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getEtag() {
        return etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public PageInfoVideo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfoVideo pageInfo) {
        this.pageInfo = pageInfo;
    }
}
