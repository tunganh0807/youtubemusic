package com.anhtung.youtubemusic.ui.main.home;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.activity.OnBackPressedCallback;
import androidx.viewpager.widget.ViewPager;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.common.Constant;
import com.anhtung.youtubemusic.common.MessageEvent;
import com.anhtung.youtubemusic.data.model.video.Item;
import com.anhtung.youtubemusic.databinding.FragmentHomeBinding;
import com.anhtung.youtubemusic.ui.adapter.ViewPageAdapter;
import com.anhtung.youtubemusic.ui.base.BaseBindingFragment;
import com.anhtung.youtubemusic.ui.main.drag_bottom.BottomFragment;
import com.anhtung.youtubemusic.ui.main.drag_top.TopFragment;
import com.tuanhav95.drag.DragView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import io.reactivex.rxjava3.annotations.NonNull;

public class HomeFragment extends BaseBindingFragment<FragmentHomeBinding, HomeViewModel> {
    private boolean checkMaxScreen = false;
    private TopFragment topFragment;
    private BottomFragment bottomFragment;
    private int heightBeforeMax = 0;

    @Override
    protected Class<HomeViewModel> getViewModel() {
        return HomeViewModel.class;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onAttach(@androidx.annotation.NonNull Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        EventBus.getDefault().unregister(this);
        super.onDetach();
    }

    @SuppressLint({"NonConstantResourceId", "UseCompatLoadingForDrawables"})
    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        topFragment = new TopFragment();
        bottomFragment = new BottomFragment();
        drag();
        heightBeforeMax = binding.dragView.getMTempHeight();

        // cơ chế view pager
        binding.viewpage.setOffscreenPageLimit(4);

        ViewPageAdapter viewPageAdapter = new ViewPageAdapter(getChildFragmentManager());
        binding.viewpage.setAdapter(viewPageAdapter);
        binding.bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.home:
                    binding.viewpage.setCurrentItem(0);
                    binding.include.tvTitle.setText(R.string.youtube_music);
                    binding.include.tvTitle.setCompoundDrawablesWithIntrinsicBounds(requireContext()
                            .getDrawable(R.drawable.ic_icon_logo), null, null, null);
                    return true;
                case R.id.trending:
                    binding.viewpage.setCurrentItem(1);
                    binding.include.tvTitle.setText(R.string.trending_videos);
                    binding.include.tvTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    return true;
                case R.id.setting:
                    binding.viewpage.setCurrentItem(2);
                    binding.include.tvTitle.setText(R.string.txt_setting);
                    binding.include.tvTitle.setCompoundDrawablesWithIntrinsicBounds(requireContext()
                            .getDrawable(R.drawable.ic_icon_logo), null, null, null);
                    return true;
                case R.id.search:
                    binding.viewpage.setCurrentItem(3);
                    binding.include.tvTitle.setText(R.string.txt_search);
                    binding.include.tvTitle.setCompoundDrawablesWithIntrinsicBounds(
                            null, null, null, null
                    );
                    return true;
            }
            return false;
        });
        binding.viewpage.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @SuppressLint("UseCompatLoadingForDrawables")
            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        binding.bottomNavigationView.getMenu().findItem(R.id.home).setChecked(true);
                        binding.include.tvTitle.setText(R.string.youtube_music);
                        binding.include.tvTitle.setCompoundDrawablesWithIntrinsicBounds(requireContext()
                                .getDrawable(R.drawable.ic_icon_logo), null, null, null);
                        break;
                    case 1:
                        binding.bottomNavigationView.getMenu().findItem(R.id.trending).setChecked(true);
                        binding.include.tvTitle.setText(R.string.trending_videos);
                        binding.include.tvTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                        break;
                    case 2:
                        binding.bottomNavigationView.getMenu().findItem(R.id.setting).setChecked(true);
                        binding.include.tvTitle.setText(R.string.youtube_music);
                        binding.include.tvTitle.setCompoundDrawablesWithIntrinsicBounds(requireContext()
                                .getDrawable(R.drawable.ic_icon_logo), null, null, null);
                        break;
                    case 3:
                        binding.bottomNavigationView.getMenu().findItem(R.id.search).setChecked(true);
                        binding.include.tvTitle.setText(R.string.txt_search);
                        binding.include.tvTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        OnBackPressedCallback callback = new OnBackPressedCallback(true /* enabled by default */) {
            @Override
            public void handleOnBackPressed() {
                if (checkMaxScreen) {
                    binding.dragView.minimize();
                    exitFullScreen();
                    checkMaxScreen = false;
                }
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);
    }

    public void hideBottomNavigation() {
        binding.bottomNavigationView.setVisibility(View.INVISIBLE);
    }

    public void showBottomNavigation() {
        binding.bottomNavigationView.setVisibility(View.VISIBLE);
    }

    //drag
    public void drag() {
        getParentFragmentManager().beginTransaction().add(R.id.frameTop, topFragment)
                .addToBackStack(null).commit();
        getParentFragmentManager().beginTransaction().add(R.id.frameBottom, bottomFragment)
                .addToBackStack(null).commit();

        if (binding.dragView.isMinimize()) {
            checkMaxScreen = false;
            showBottomNavigation();
        } else if (binding.dragView.isMaximize()) {
            checkMaxScreen = true;
            hideBottomNavigation();
        }

        binding.dragView.setDragListener(new DragView.DragListener() {
            @Override
            public void onExpanded() {

            }

            @Override
            public void onChangeState(@NonNull DragView.State state) {
                if (!checkMaxScreen) {
                    topFragment.hideIconVideo();
                } else {
                    topFragment.showIconVideo();
                }
            }

            @Override
            public void onChangePercent(float percent) {

            }
        });
    }

    public void showDragView(Item result) {
        if (result != null) {
            topFragment.initYoutubeScreen(result.getId());
            binding.dragView.maximize();
            bottomFragment.initDataVideo(result);
            bottomFragment.initDataSearch(result);
            bottomFragment.commentInfo(result);
            mainViewModel.stringMutableLiveData.setValue(result.getId());

            String titleVideo = result.getSnippet().getTitle();
            String channelVideo = result.getSnippet().getChannelTitle();
            String imgVideo = result.getSnippet().getThumbnails().getHigh().getUrl();

            EventBus.getDefault().post(new MessageEvent(Constant.TYPE_VIDEO, titleVideo));
            EventBus.getDefault().post(new MessageEvent(Constant.TYPE_CHANNEL, channelVideo));
            EventBus.getDefault().post(new MessageEvent(Constant.TYPE_IMG, imgVideo));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShowMyDesign(MessageEvent event) {
        switch (event.getTypeEvent()) {
            case Constant.HIDE_NAVIGATION:
                hideBottomNavigation();
                checkMaxScreen = true;
                break;
            case Constant.SHOW_NAVIGATION:
                showBottomNavigation();
                checkMaxScreen = false;
                break;
            case Constant.ENTER_FULL_SCREEN:
                binding.dragView.setHeightMax(ViewGroup.LayoutParams.MATCH_PARENT, true);
                break;
            case Constant.EXIT_FULL_SCREEN:
                binding.dragView.setHeightMax(heightBeforeMax, true);

                break;
            case Constant.SEEK_BAR:
                heightBeforeMax = binding.dragView.getMTempHeight() + 72;
                break;
            case Constant.HIDE_NAVIGATION_TO_WEB_VIEW:
                hideBottomNavigation();
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShowDrag(MessageEvent event) {
        Item item = event.getItem();
        if (item != null)
            showDragView(item);
    }

    @Override
    protected void onPermissionGranted() {

    }

    public void exitFullScreen() {
        if (heightBeforeMax > 0) {
            binding.dragView.setHeightMax(heightBeforeMax, true);
        }
    }

    @SuppressLint("InlinedApi")
    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        int orientation = newConfig.orientation;
        if (checkMaxScreen) {
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                View decorView = requireActivity().getWindow().getDecorView();
                decorView.setSystemUiVisibility(0);
                requireActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                exitFullScreen();
                requireActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            } else if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                // ẩn thanh bottom bar khi xoay màn hình
                Window window = requireActivity().getWindow();
                window.getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LOW_PROFILE
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                );
                window.setStatusBarColor(Color.TRANSPARENT);
                binding.dragView.setHeightMax(ViewGroup.LayoutParams.MATCH_PARENT, true);
                requireActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            }
        }
    }
}



