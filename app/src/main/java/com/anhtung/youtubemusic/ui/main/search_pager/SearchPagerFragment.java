package com.anhtung.youtubemusic.ui.main.search_pager;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.data.local.entities.Suggestion;
import com.anhtung.youtubemusic.data.model.video.Item;
import com.anhtung.youtubemusic.data.respository.SuggestionRepository;
import com.anhtung.youtubemusic.databinding.SearchPagerFragmentBinding;
import com.anhtung.youtubemusic.ui.adapter.RecentSearchAdapter;
import com.anhtung.youtubemusic.ui.adapter.TitleSearchAdapter;
import com.anhtung.youtubemusic.ui.adapter.VideoSearchAdapter;
import com.anhtung.youtubemusic.ui.base.BaseBindingFragment;
import com.anhtung.youtubemusic.ui.main.home.HomeFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class SearchPagerFragment extends BaseBindingFragment<SearchPagerFragmentBinding, SearchPagerViewModel> {
    ActivityResultLauncher<Intent> launcherVoice = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    if (data != null) {
                        ArrayList<String> results = data.getStringArrayListExtra(
                                RecognizerIntent.EXTRA_RESULTS);
                        binding.search.setQuery(results.get(0), true);
                        handleVideoSearch(results.get(0));
                    }
                }
            });
    private Context context;
    private TitleSearchAdapter adapter;
    private VideoSearchAdapter mAdapter;
    private RecentSearchAdapter rAdapter;
    private List<Suggestion> listData;
    private String storage;

    @Override
    protected Class<SearchPagerViewModel> getViewModel() {
        return SearchPagerViewModel.class;
    }

    @Override
    public int getLayoutId() {
        return R.layout.search_pager_fragment;
    }

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        observerData();
        handleSearchView();
    }

    private void handleSearchView() {
        handleVoice();

        binding.search.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                handleTitleSearch(query);
                handleVideoSearch(query);
                handleRecentSearch(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                handleTitleSearch(newText);
                return true;
            }
        });
    }

    private void handleVoice() {
        binding.imgVoice.setOnClickListener(v -> {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
            launcherVoice.launch(intent);
        });
    }


    private void handleRecentSearch(String query) {
        if (!query.isEmpty() || !query.equals("")) {
            String str = query.toLowerCase();
            binding.imgVoice.setVisibility(View.GONE);

            SuggestionRepository.getInstance().getListSuggestion(data -> listData = (List<Suggestion>) data);
            if (listData == null) {
                Log.d("tung", "handleRecentSearch:add null " + str);
                viewModel.addSuggestion(new Suggestion(str));
            } else {
                for (int i = 0; i < listData.size(); i++) {
                    if (str.equals(listData.get(i).name)) {
                        return;
                    } else if(!str.equals(listData.get(i).name)) {
                        storage = str;
                    }
                }
                viewModel.addSuggestion(new Suggestion(storage));
            }

        } else {
            binding.imgVoice.setVisibility(View.GONE);
        }
    }

    @SuppressLint("SetTextI18n")
    private void handleVideoSearch(String query) {
        if (!query.isEmpty() || !query.equals("")) {
            viewModel.getSearchVideo(query);
            binding.tvRecent.setText("");
            binding.prRing.setVisibility(View.VISIBLE);
            binding.rvVideoHistory.setVisibility(View.GONE);
            binding.rvTitleSearch.setVisibility(View.GONE);
            binding.rvVideoSearch.setVisibility(View.GONE);
        } else {
            binding.imgVoice.setVisibility(View.GONE);
        }
    }

    @SuppressLint("SetTextI18n")
    private void handleTitleSearch(String str) {
        if (!str.isEmpty() || !str.equals("")) {
            viewModel.getVideoSearch(str);
            binding.tvRecent.setText(R.string.txt_search);
            binding.imgVoice.setVisibility(View.GONE);

            binding.rvVideoHistory.setVisibility(View.GONE);
            binding.rvVideoSearch.setVisibility(View.GONE);
            binding.rvTitleSearch.setVisibility(View.VISIBLE);
        } else {
            binding.tvRecent.setText(R.string.tv_recent);
            binding.rvVideoSearch.setVisibility(View.GONE);
            binding.rvTitleSearch.setVisibility(View.GONE);
            binding.imgVoice.setVisibility(View.GONE);
            binding.rvVideoHistory.setVisibility(View.VISIBLE);
            getListRecentSearch();
        }
    }

    private void observerData() {
        notifyException();

        getListTitleSearch();
        getListVideoSearch();
        getListRecentSearch();
    }

    private void notifyException() {
        viewModel.sms.observe(getViewLifecycleOwner(), s ->
                Toast.makeText(getContext(), s, Toast.LENGTH_LONG).show()
        );
    }

    public void getListRecentSearch() {
        SuggestionRepository.getInstance().getListSuggestion(data -> {
            List<Suggestion> list = (List<Suggestion>) data;
            Collections.reverse(list);

            requireActivity().runOnUiThread(() -> {
                rAdapter = new RecentSearchAdapter(requireContext());
                binding.rvVideoHistory.setAdapter(rAdapter);
                rAdapter.setListSuggestion(list);

                rAdapter.getItemResult().observe(getViewLifecycleOwner(), this::handleVideoSearch);
            });

        });
    }

    private void getListVideoSearch() {
        mAdapter = new VideoSearchAdapter(context);
        binding.rvVideoSearch.setAdapter(mAdapter);
        viewModel.searchVideo.observe(getViewLifecycleOwner(), search -> {
            binding.prRing.setVisibility(View.GONE);
            binding.rvVideoSearch.setVisibility(View.VISIBLE);
            mAdapter.setList(search.getItems());
        });
        mAdapter.getItemResult().observe(getViewLifecycleOwner(), this::handleItemResult);
    }

    private void getListTitleSearch() {
        adapter = new TitleSearchAdapter(context);
        binding.rvTitleSearch.setAdapter(adapter);
        viewModel.newSearch.observe(getViewLifecycleOwner(), search -> {
            if (search != null) {
                adapter.setListItem(search.getItems());
            }
        });
        adapter.getItemResult().observe(getViewLifecycleOwner(), this::handleItemResult);
    }

    private void handleItemResult(Item result) {
        if (result == null) return;
        ((HomeFragment) requireParentFragment()).showDragView(result);
    }

    @Override
    protected void onPermissionGranted() {

    }
}
