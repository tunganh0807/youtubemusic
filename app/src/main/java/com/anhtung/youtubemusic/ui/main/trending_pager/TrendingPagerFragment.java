package com.anhtung.youtubemusic.ui.main.trending_pager;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.data.model.video.Item;
import com.anhtung.youtubemusic.databinding.TrendingPagerFragmentBinding;
import com.anhtung.youtubemusic.ui.adapter.VideoAdapter;
import com.anhtung.youtubemusic.ui.base.BaseBindingFragment;
import com.anhtung.youtubemusic.ui.main.home.HomeFragment;

public class TrendingPagerFragment extends BaseBindingFragment<TrendingPagerFragmentBinding, TrendingPagerViewModel> {
    private Context context;
    private VideoAdapter adapter;

    @Override
    protected Class<TrendingPagerViewModel> getViewModel() {
        return TrendingPagerViewModel.class;
    }

    @Override
    public int getLayoutId() {
        return R.layout.trending_pager_fragment;
    }

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        initData();
        observerData();
    }

    private void observerData() {
        notifyException();

        LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext());
        binding.listItem.setLayoutManager(layoutManager);
        if(adapter == null) {
            adapter = new VideoAdapter(context);
            binding.listItem.setAdapter(adapter);
            viewModel.videoMutableLiveData.observe(getViewLifecycleOwner(), video -> {
                binding.prRing.setVisibility(View.GONE);
                binding.listItem.setVisibility(View.VISIBLE);
                adapter.setListItem(video.getItems());
            });
            adapter.getItemResult().observe(getViewLifecycleOwner(), this::handleItemResult);
        }
    }

    private void notifyException() {
        viewModel.sms.observe(getViewLifecycleOwner(), s ->
                Toast.makeText(getContext(), s, Toast.LENGTH_LONG).show()
        );
    }

    private void handleItemResult(Item result) {
        if(result == null){
            Toast.makeText(requireContext(), R.string.data_is_loading, Toast.LENGTH_SHORT).show();
        }else{
            ((HomeFragment) requireParentFragment()).showDragView(result);
        }
    }

    private void initData() {
        viewModel.getVideo();
    }

    @Override
    protected void onPermissionGranted() {

    }
}

