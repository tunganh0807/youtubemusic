package com.anhtung.youtubemusic.ui.main.home_pager;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.data.model.video.Item;
import com.anhtung.youtubemusic.databinding.HomePagerFragmentBinding;
import com.anhtung.youtubemusic.ui.adapter.SearchAdapter;
import com.anhtung.youtubemusic.ui.base.BaseBindingFragment;
import com.anhtung.youtubemusic.ui.main.home.HomeFragment;

public class HomePagerFragment extends BaseBindingFragment<HomePagerFragmentBinding, HomePagerViewModel> {
    private Context context;

    @Override
    protected Class<HomePagerViewModel> getViewModel() {
        return HomePagerViewModel.class;
    }

    @Override
    public int getLayoutId() {
        return R.layout.home_pager_fragment;
    }

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        observerData();
        initData();
    }

    private void observerData() {
        notifyException();

        SearchAdapter adapter = new SearchAdapter(context);
        LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext());
        binding.listItem.setLayoutManager(layoutManager);
        binding.listItem.setAdapter(adapter);


        viewModel.itemVideoSearch.observe(getViewLifecycleOwner(), search -> {
            binding.prRing.setVisibility(View.GONE);
            binding.listItem.setVisibility(View.VISIBLE);
            adapter.setListItem(search.getItems());
        });
        adapter.getItemResult().observe(getViewLifecycleOwner(), this::handleItemResult);
    }

    private void notifyException() {
        viewModel.sms.observe(getViewLifecycleOwner(), s ->
                Toast.makeText(getContext(), s, Toast.LENGTH_LONG).show()
        );
    }

    private void handleItemResult(Item item) {
        if (item == null) return;
        ((HomeFragment) requireParentFragment()).showDragView(item);
    }

    private void initData() {
        viewModel.getVideoSearch();

    }

    @Override
    protected void onPermissionGranted() {

    }
}

