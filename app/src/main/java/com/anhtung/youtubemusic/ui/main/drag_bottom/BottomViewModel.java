package com.anhtung.youtubemusic.ui.main.drag_bottom;

import androidx.lifecycle.MutableLiveData;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.common.Constant;
import com.anhtung.youtubemusic.data.model.item_video.ItemVideo;
import com.anhtung.youtubemusic.data.model.search.Search;
import com.anhtung.youtubemusic.data.model.video.Item;
import com.anhtung.youtubemusic.data.respository.YoutubeRepository;
import com.anhtung.youtubemusic.ui.base.BaseViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

@HiltViewModel
public class BottomViewModel extends BaseViewModel {
    private final YoutubeRepository youtubeRepository;
    MutableLiveData<ItemVideo> itemVideoMutableLiveData = new MutableLiveData<>();
    MutableLiveData<ItemVideo> commentMutableLiveData = new MutableLiveData<>();
    MutableLiveData<Search> itemFixSearch = new MutableLiveData<>();
    MutableLiveData<Integer> sms = new MutableLiveData<>();

    @Inject
    public BottomViewModel(YoutubeRepository youtubeRepository) {
        this.youtubeRepository = youtubeRepository;
    }

    public void getItemVideo(Item item) {
        youtubeRepository.getItemVideo(item.getId(), Constant.API_KEY)
                .subscribe(new SingleObserver<ItemVideo>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(@NonNull ItemVideo itemVideo) {
                        itemVideoMutableLiveData.postValue(itemVideo);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        sms.postValue(R.string.key_error);
                    }
                });
    }

    public void getCommentYoutube(String id) {
        youtubeRepository.getItemVideo3(id, Constant.API_KEY)
                .subscribe(new SingleObserver<ItemVideo>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(@NonNull ItemVideo itemVideo) {
                        commentMutableLiveData.postValue(itemVideo);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        sms.postValue(R.string.key_error);
                    }
                });
    }

    public void getFixSearchApi(Item item) {
        youtubeRepository.fixSearchApi(Constant.MAX_RESULT, item.getSnippet().getTitle(),
                Constant.VIDEO, Constant.API_KEY).subscribe(new SingleObserver<Search>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(@NonNull Search search) {
                itemFixSearch.postValue(search);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                sms.postValue(R.string.key_error);
            }
        });
    }
}

