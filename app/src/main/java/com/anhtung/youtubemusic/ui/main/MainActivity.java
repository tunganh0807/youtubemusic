package com.anhtung.youtubemusic.ui.main;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.RequiresApi;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.fragment.NavHostFragment;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.common.Constant;
import com.anhtung.youtubemusic.common.MessageEvent;
import com.anhtung.youtubemusic.databinding.ActivityMainBinding;
import com.anhtung.youtubemusic.ui.base.BaseBindingActivity;
import com.anhtung.youtubemusic.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MainActivity extends BaseBindingActivity<ActivityMainBinding, MainViewModel> {
    public NavController navControllerMain;
    public NavHostFragment navHostFragmentMain;
    private NavGraph graph;

    private NotificationManager notificationManager;
    private String channelVideo;
    private String titleVideo;
    private String imageVideo;

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public Class<MainViewModel> getViewModel() {
        return MainViewModel.class;
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void setupView(Bundle savedInstanceState) {
        navHostFragmentMain = (NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentContainerView);
        if (navHostFragmentMain != null) {
            navControllerMain = navHostFragmentMain.getNavController();
        }
        Handler handler = new Handler(this.getMainLooper());
        handler.postDelayed(() -> changeMainScreen(R.id.homeFragment, null), 2000);

    }

    public void changeMainScreen(int idScreen, Bundle bundle) {
        if (graph == null) {
            graph = navControllerMain.getNavInflater().inflate(R.navigation.nav_main);
        }
        graph.setStartDestination(idScreen);
        navControllerMain.setGraph(graph, bundle);
    }

    @Override
    public void setupData() {

    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void showNotification() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);

        NotificationChannel notificationChannel = new NotificationChannel(Constant.CHANNEL_ID, Constant.PRIORITY_CHANNEL_ID, NotificationManager.IMPORTANCE_LOW);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), Constant.REQUEST_CODE_1, intent, 0);
        Notification notification = new Notification.Builder(getApplicationContext(), Constant.CHANNEL_ID).setLargeIcon(Utils.getIcon(imageVideo))
                .setContentText(channelVideo)
                .setContentTitle(titleVideo)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_shape_icon)
                .build();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(notificationChannel);
        notificationManager.notify(Constant.NOTIFICATION_ID_1, notification);
    }

    public void closeNotification() {
        if (notificationManager != null) {
            notificationManager.cancel(Constant.NOTIFICATION_ID_1);
        }
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 2) {
            super.onBackPressed();
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShowMyDesign(MessageEvent event) {
        switch (event.getTypeEvent()) {
            case Constant.TYPE_VIDEO:
                titleVideo = event.getStringValue();
                break;
            case Constant.TYPE_CHANNEL:
                channelVideo = event.getStringValue();
                break;
            case Constant.TYPE_IMG:
                imageVideo = event.getStringValue();
                break;
        }
    }

}