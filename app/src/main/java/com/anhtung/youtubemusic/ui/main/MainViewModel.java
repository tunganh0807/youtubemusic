package com.anhtung.youtubemusic.ui.main;

import androidx.lifecycle.MutableLiveData;

import com.anhtung.youtubemusic.ui.base.BaseViewModel;

import javax.inject.Inject;

public class MainViewModel extends BaseViewModel {
    public MutableLiveData<String> stringMutableLiveData=new MutableLiveData<>();
    @Inject
    public MainViewModel() {
    }
}
