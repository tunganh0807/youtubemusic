package com.anhtung.youtubemusic.ui.main.home;

import com.anhtung.youtubemusic.ui.base.BaseViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class HomeViewModel extends BaseViewModel {
    @Inject
    public HomeViewModel() {
    }
}
