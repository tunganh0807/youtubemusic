package com.anhtung.youtubemusic.ui.main.setting_pager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.common.Constant;
import com.anhtung.youtubemusic.databinding.SettingPagerFragmentBinding;
import com.anhtung.youtubemusic.ui.base.BaseBindingFragment;
import com.anhtung.youtubemusic.ui.dialog.LanguageDialog;
import com.anhtung.youtubemusic.ui.dialog.PlayBackgroundDialog;

import timber.log.Timber;

public class SettingPagerFragment extends BaseBindingFragment<SettingPagerFragmentBinding,
        SettingPagerViewModel> implements View.OnClickListener {
    private static final String TAG = SettingPagerFragment.class.getName();

    @Override
    protected Class<SettingPagerViewModel> getViewModel() {
        return SettingPagerViewModel.class;
    }

    @Override
    public int getLayoutId() {
        return R.layout.setting_pager_fragment;
    }

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        binding.tvLanguage.setOnClickListener(this);
        binding.tvBackground.setOnClickListener(this);
        binding.tvPolicy.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_language) {
            getLanguage();
        } else if (v.getId() == R.id.tv_background) {
            getPlayBackground();
        }  else if (v.getId() == R.id.tv_policy) {
            getPolicy();
        }
    }

    private void getPolicy() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.LINK_GOOGLE));
        startActivity(browserIntent);
    }

    private void showDialogConfirm(int type) {
        PlayBackgroundDialog dialogFragment = new PlayBackgroundDialog();
        dialogFragment.setType(type);
        dialogFragment.show(requireActivity().getSupportFragmentManager(), null);
    }

    private void getPlayBackground() {
        showDialogConfirm(Constant.TYPE_HOME);
    }

    private void getLanguage() {
        LanguageDialog dialog = new LanguageDialog();
        dialog.show(requireActivity().getSupportFragmentManager(), null);
    }

    @Override
    protected void onPermissionGranted() {

    }
}

