package com.anhtung.youtubemusic.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.data.model.search.ItemSearch;
import com.anhtung.youtubemusic.data.model.video.Item;
import com.anhtung.youtubemusic.databinding.ItemHomePagerBinding;
import com.anhtung.youtubemusic.utils.ConvertCount;
import com.bumptech.glide.Glide;

import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchHolder> {
    private final Context mContext;
    private final MutableLiveData<Item> itemResult = new MutableLiveData<>();
    private List<ItemSearch> listItem;

    public SearchAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setListItem(List<ItemSearch> listItem) {
        this.listItem = listItem;
        notifyDataSetChanged();
    }

    public List<ItemSearch> getListItem() {
        return listItem;
    }

    @NonNull
    @Override
    public SearchHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemHomePagerBinding viewBinding = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_home_pager, parent, false);
        return new SearchHolder(viewBinding);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull SearchHolder holder, int position) {
        holder.bind(listItem.get(position));
    }

    @Override
    public int getItemCount() {
        if (listItem != null) {
            return listItem.size();
        }
        return 0;
    }

    public class SearchHolder extends RecyclerView.ViewHolder {
        ItemHomePagerBinding viewBinding;

        public SearchHolder(@NonNull ItemHomePagerBinding v) {
            super(v.getRoot());
            viewBinding = v;
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        public void bind(ItemSearch item) {
            Item itemVideo = item.getVideoItem().getItems().get(0);

            if (item.getSnippet().getThumbnails().getHigh().getUrl() == null) {
                Glide.with(viewBinding.ivThumbnails).load(R.drawable.ic_launcher_background)
                        .override(375, 216).into(viewBinding.ivThumbnails);
            } else {
                Glide.with(viewBinding.ivThumbnails).load(item.getSnippet().getThumbnails()
                        .getHigh().getUrl()).override(375, 216).into(viewBinding.ivThumbnails);
            }

            Glide.with(viewBinding.ivChannelIcon).load(itemVideo.getChannel().getItems().get(0)
                    .getSnippet().getThumbnails().getHigh().getUrl())
                    .into(viewBinding.ivChannelIcon);


            viewBinding.tvTitle.setTag(itemVideo);
            viewBinding.tvTitle.setText(item.getSnippet().getTitle());
            viewBinding.tvTime.setText(ConvertCount.convertTime(
                    viewBinding.getRoot().getContext(), item.getSnippet().getPublishedAt())
            );
            viewBinding.tvViewCount.setText(ConvertCount.convertViewCount(
                    viewBinding.getRoot().getContext(), itemVideo.getStatistics().getViewCount())
            );
            viewBinding.tvChannelTitle.setText(item.getSnippet().getChannelTitle());

            viewBinding.lnVideo.setOnClickListener(v ->
                    showDetailVideo(viewBinding.tvTitle.getTag())
            );
        }
    }

    private void showDetailVideo(Object tag) {

        itemResult.postValue((Item) tag);
    }

    public MutableLiveData<Item> getItemResult() {
        return itemResult;
    }
}

