package com.anhtung.youtubemusic.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.data.model.search.ItemSearch;
import com.anhtung.youtubemusic.data.model.video.Item;
import com.anhtung.youtubemusic.databinding.ItemVideoSearchBinding;
import com.anhtung.youtubemusic.utils.ConvertCount;
import com.bumptech.glide.Glide;

import java.time.Duration;
import java.util.List;

public class VideoSearchAdapter extends RecyclerView.Adapter<VideoSearchAdapter.VideoSearchHolder> {
    private final Context mContext;
    private List<ItemSearch> list;
    private final MutableLiveData<Item> itemResult = new MutableLiveData<>();

    public VideoSearchAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setList(List<ItemSearch> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VideoSearchHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemVideoSearchBinding viewBinding = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_video_search, parent, false);
        return new VideoSearchHolder(viewBinding);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull VideoSearchHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public class VideoSearchHolder extends RecyclerView.ViewHolder {
        ItemVideoSearchBinding viewBinding;

        public VideoSearchHolder(@NonNull ItemVideoSearchBinding v) {
            super(v.getRoot());
            viewBinding = v;
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        public void bind(ItemSearch item) {
            if (item.getSnippet().getThumbnails().getHigh().getUrl() == null) {
                Glide.with(viewBinding.ivThumbnails).load(R.drawable.bg_color_icon)
                        .override(375, 216).into(viewBinding.ivThumbnails);
            } else {
                Glide.with(viewBinding.ivThumbnails).load(item.getSnippet().getThumbnails()
                        .getHigh().getUrl()).override(375, 216).into(viewBinding.ivThumbnails);
            }

            viewBinding.tvTitle.setTag(item.getVideoItem().getItems().get(0));
            viewBinding.tvTitle.setText(item.getSnippet().getTitle());
            viewBinding.tvChannel.setText(item.getSnippet().getChannelTitle());
            viewBinding.tvTime.setText(ConvertCount.convertTime(viewBinding.getRoot().getContext(),
                    item.getSnippet().getPublishedAt())
            );
            viewBinding.tvDuration.setText(ConvertCount.convertDuration(Duration.parse(
                    item.getVideoItem().getItems().get(0).getContentDetails().getDuration())
                    .getSeconds())
            );
            viewBinding.tvViewCount.setText(ConvertCount.convertViewCount(
                    viewBinding.getRoot().getContext(), item.getVideoItem().getItems().get(0)
                            .getStatistics().getViewCount())
            );
            viewBinding.lnVideo.setOnClickListener(v ->
                    showDetailVideo(viewBinding.tvTitle.getTag())
            );
        }
    }

    private void showDetailVideo(Object tag) {
        itemResult.setValue((Item) tag);
    }

    public MutableLiveData<Item> getItemResult() {
        return itemResult;
    }
}

