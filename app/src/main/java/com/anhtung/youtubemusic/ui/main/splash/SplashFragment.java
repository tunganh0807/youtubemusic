package com.anhtung.youtubemusic.ui.main.splash;

import static android.view.View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.databinding.FragmentSplashBinding;
import com.anhtung.youtubemusic.ui.base.BaseBindingFragment;

public class SplashFragment extends BaseBindingFragment<FragmentSplashBinding, SplashViewModel> {
    @Override
    protected Class<SplashViewModel> getViewModel() {
        return SplashViewModel.class;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_splash;
    }

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        Window window = requireActivity().getWindow();
        window.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        );
        window.setStatusBarColor(Color.TRANSPARENT);
    }

    @Override
    protected void onPermissionGranted() {

    }

    @SuppressLint("InlinedApi")
    @Override
    public void onDestroy() {
        View decorView = requireActivity().getWindow().getDecorView();
        decorView.setSystemUiVisibility(0);
        requireActivity().getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        super.onDestroy();
    }
}

