package com.anhtung.youtubemusic.ui.main.setting_pager;

import com.anhtung.youtubemusic.ui.base.BaseViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class SettingPagerViewModel extends BaseViewModel {
    @Inject
    public SettingPagerViewModel() {
    }
}
