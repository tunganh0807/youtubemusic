package com.anhtung.youtubemusic.ui.main.comment;

import androidx.lifecycle.MutableLiveData;

import com.anhtung.youtubemusic.common.Constant;
import com.anhtung.youtubemusic.data.model.item_video.ItemVideo;
import com.anhtung.youtubemusic.data.respository.YoutubeRepository;
import com.anhtung.youtubemusic.ui.base.BaseViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

@HiltViewModel
public class CommentViewModel extends BaseViewModel {
    private final YoutubeRepository youtubeRepository;
    MutableLiveData<ItemVideo> commentMutableLiveData = new MutableLiveData<>();

    @Inject
    public CommentViewModel(YoutubeRepository youtubeRepository) {
        this.youtubeRepository = youtubeRepository;
    }

    public void getCommentYoutube(String id){
        youtubeRepository.getItemVideo3(id, Constant.API_KEY)
                .subscribe(new SingleObserver<ItemVideo>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(@NonNull ItemVideo itemVideo) {
                        commentMutableLiveData.postValue(itemVideo);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                    }
                });
    }
}
