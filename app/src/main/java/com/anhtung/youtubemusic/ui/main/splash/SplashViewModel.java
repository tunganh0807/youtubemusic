package com.anhtung.youtubemusic.ui.main.splash;

import com.anhtung.youtubemusic.ui.base.BaseViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class SplashViewModel extends BaseViewModel {
    @Inject
    public SplashViewModel() {
    }
}
