package com.anhtung.youtubemusic.ui.main.drag_bottom;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.common.Constant;
import com.anhtung.youtubemusic.common.MessageEvent;
import com.anhtung.youtubemusic.data.model.comment.ItemComment;
import com.anhtung.youtubemusic.data.model.item_video.ItemVideo;
import com.anhtung.youtubemusic.data.model.item_video.ItemVideoItem;
import com.anhtung.youtubemusic.data.model.search.ItemSearch;
import com.anhtung.youtubemusic.data.model.video.Item;
import com.anhtung.youtubemusic.databinding.FragmentBottomBinding;
import com.anhtung.youtubemusic.ui.adapter.SearchAdapter;
import com.anhtung.youtubemusic.ui.base.BaseBindingFragment;
import com.anhtung.youtubemusic.ui.dialog.ProgressDialog;
import com.anhtung.youtubemusic.ui.main.comment.CommentFragment;
import com.anhtung.youtubemusic.utils.ConvertCount;
import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class BottomFragment extends BaseBindingFragment<FragmentBottomBinding, BottomViewModel>
        implements View.OnClickListener {
    public static final String TAG = BottomFragment.class.getName();
    private final List<Item> listItem = new ArrayList<>();
    ProgressDialog dialog;
    private SearchAdapter adapter;
    private Context context;
    private String videoId;
    private Item itemNext;

    @Override
    protected Class<BottomViewModel> getViewModel() {
        return BottomViewModel.class;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_bottom;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        observerData();
        binding.ivShowComment.setOnClickListener(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_show_comment) {
            showComment();
        }
    }

    private void showComment() {
        FragmentTransaction fragmentTransaction = getParentFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.frameBottom, new CommentFragment(), TAG);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    protected void onPermissionGranted() {

    }

    public void initDataVideo(Item item) {
        dialog = new ProgressDialog();
        dialog.show(getChildFragmentManager(), null);

        if (item == null) {
            Toast.makeText(requireContext(), R.string.txt_empty_data, Toast.LENGTH_SHORT).show();
            return;
        }
        viewModel.getItemVideo(item);
    }

    private void observerData() {
        notifyException();
        viewModel.itemVideoMutableLiveData.observe(getViewLifecycleOwner(), this::initInfoVideo);
        viewModel.commentMutableLiveData.observe(getViewLifecycleOwner(), new Observer<ItemVideo>() {
            @Override
            public void onChanged(ItemVideo itemVideo) {
                infoComment(itemVideo);
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext());
        binding.list.setLayoutManager(layoutManager);
        if (adapter == null) {
            adapter = new SearchAdapter(context);
            binding.list.setAdapter(adapter);

            viewModel.itemFixSearch.observe(getViewLifecycleOwner(), search -> {
                // check video id sample
                List<ItemSearch> list = search.getItems();
                if (list != null && list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        ItemSearch item = list.get(i);
                        if (item.getId() != null && item.getId().getVideoId().equals(videoId)) {
                            list.remove(item);
                        }
                    }
                    adapter.setListItem(list);
                    itemNext = list.get(0).getVideoItem().getItems().get(0);
                }

                dialog.dismiss();
            });
            adapter.getItemResult().observe(getViewLifecycleOwner(), this::handleItemResult);
        }
    }

    private void notifyException() {
        viewModel.sms.observe(getViewLifecycleOwner(), s ->
                Toast.makeText(getContext(), s, Toast.LENGTH_LONG).show()
        );
    }

    private void infoComment(ItemVideo itemVideo) {
        ItemComment item = itemVideo.getItems().get(0).getComment().getItems().get(0);
        Glide.with(binding.ivIconHuman).load(item.getSnippet().getTopLevelComment()
                .getSnippet().getAuthorProfileImageUrl()).into(binding.ivIconHuman);
        binding.tvCommentHuman.setText(item.getSnippet().getTopLevelComment()
                .getSnippet().getTextOriginal()
        );
    }

    @SuppressLint("TimberArgCount")
    private void handleItemResult(Item result) {
        if (result == null) {
            Toast.makeText(requireContext(), R.string.txt_empty_data, Toast.LENGTH_SHORT).show();
        } else {
            EventBus.getDefault().post(new MessageEvent(result));
            Timber.e("Result is not null", TAG);
        }
    }

    private void initInfoVideo(ItemVideo itemVideo) {
        ItemVideoItem item = itemVideo.getItems().get(0);
        videoId = item.getId();

        Glide.with(binding.ivChannelIcon).load(item.getChannel().getItems().get(0)
                .getSnippet().getThumbnails().getHigh().getUrl()).into(binding.ivChannelIcon);

        binding.tvTitle.setText(item.getSnippet().getTitle());
        binding.tvViewCount.setText(ConvertCount.convertViewCount(getContext(),
                item.getStatistics().getViewCount()));
        binding.tvTime.setText(ConvertCount.convertTime(getContext(),
                item.getSnippet().getPublishedAt()));
        binding.tvLike.setText(ConvertCount.convertLikeCount(item));
        binding.tvDislike.setText(ConvertCount.convertDisLikeCount(item));
        binding.tvChannelTitle.setText(item.getSnippet().getChannelTitle());
        binding.tvViewCount2.setText(ConvertCount.convertViewCount(getContext(),
                item.getStatistics().getViewCount()));
        binding.tvTime2.setText(ConvertCount.convertTime(getContext(),
                item.getSnippet().getPublishedAt()));
        binding.tvCommentCount.setText(ConvertCount.convertCommentCount(item));

        EventBus.getDefault().post(new MessageEvent(Constant.TYPE_TITLE, item.getSnippet().getTitle()));
        EventBus.getDefault().post(new MessageEvent(Constant.TYPE_TITLE_CHANNEL, item.getSnippet().getChannelTitle()));

    }

    public void initDataSearch(Item result) {
        if (result == null) {
            Toast.makeText(requireContext(), R.string.txt_empty_data, Toast.LENGTH_SHORT).show();
            return;
        }
        viewModel.getFixSearchApi(result);
    }

    public void commentInfo(Item result) {
        if (result == null) {
            Toast.makeText(requireContext(), R.string.txt_empty_data, Toast.LENGTH_SHORT).show();
            return;
        }
        viewModel.getCommentYoutube(result.getId());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShowMyDesign(MessageEvent event) {
        if (event.getTypeEvent() == Constant.NEXT_VIDEO) {
            EventBus.getDefault().post(new MessageEvent(itemNext));

            listItem.add(itemNext);
            MessageEvent messageEvent = new MessageEvent(listItem);
            messageEvent.setTypeEvent(Constant.PREVIOUS_VIDEO);
            EventBus.getDefault().post(messageEvent);
        }
    }
}
