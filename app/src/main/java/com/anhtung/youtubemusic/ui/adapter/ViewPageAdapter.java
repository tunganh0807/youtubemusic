package com.anhtung.youtubemusic.ui.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.anhtung.youtubemusic.ui.main.home_pager.HomePagerFragment;
import com.anhtung.youtubemusic.ui.main.search_pager.SearchPagerFragment;
import com.anhtung.youtubemusic.ui.main.setting_pager.SettingPagerFragment;
import com.anhtung.youtubemusic.ui.main.trending_pager.TrendingPagerFragment;

public class ViewPageAdapter extends FragmentStatePagerAdapter {
    private HomePagerFragment homePagerFragment;
    private TrendingPagerFragment trendingPagerFragment;
    private SettingPagerFragment settingPagerFragment;
    private SearchPagerFragment searchPagerFragment;

    public ViewPageAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                if (homePagerFragment == null) homePagerFragment = new HomePagerFragment();
                return homePagerFragment;
            case 1:
                if (trendingPagerFragment == null) trendingPagerFragment = new TrendingPagerFragment();
                return trendingPagerFragment;
            case 2:
                if (settingPagerFragment == null) settingPagerFragment = new SettingPagerFragment();
                return settingPagerFragment;
            case 3:
            default:
                if (searchPagerFragment == null) searchPagerFragment = new SearchPagerFragment();
                return searchPagerFragment;
        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}

