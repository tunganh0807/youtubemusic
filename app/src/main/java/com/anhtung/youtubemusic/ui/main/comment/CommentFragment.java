package com.anhtung.youtubemusic.ui.main.comment;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.databinding.FragmentCommentBinding;
import com.anhtung.youtubemusic.ui.adapter.CommentAdapter;
import com.anhtung.youtubemusic.ui.base.BaseBindingFragment;
import com.anhtung.youtubemusic.ui.main.drag_bottom.BottomFragment;
import com.anhtung.youtubemusic.utils.ConvertCount;

public class CommentFragment extends BaseBindingFragment<FragmentCommentBinding, CommentViewModel> {
    private Context context;
    private CommentAdapter adapter;
    private String id;

    @Override
    protected Class<CommentViewModel> getViewModel() {
        return CommentViewModel.class;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_comment;
    }

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        binding.ivClose.setOnClickListener(v -> closeComment());
        observerData();
    }

    private void observerData() {
        mainViewModel.stringMutableLiveData.observe(getViewLifecycleOwner(), s ->
                viewModel.getCommentYoutube(s)
        );

        LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext());
        binding.rvComment.setLayoutManager(layoutManager);
        if (adapter == null) {
            adapter = new CommentAdapter(context);
            binding.rvComment.setAdapter(adapter);
            viewModel.commentMutableLiveData.observe(getViewLifecycleOwner(), itemVideo -> {
                binding.prRing.setVisibility(View.GONE);
                binding.rvComment.setVisibility(View.VISIBLE);

                adapter.setListItem(itemVideo.getItems().get(0).getComment().getItems());
                binding.tvCommentCount.setText(ConvertCount.convertCommentCount(itemVideo.getItems().get(0)));
            });
        }
    }

    private void closeComment() {
        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();
        Fragment commentFragment = getParentFragmentManager().findFragmentByTag(BottomFragment.TAG);
        if (commentFragment != null) {
            transaction.remove(commentFragment).commit();
        }
    }

    @Override
    protected void onPermissionGranted() {

    }
}
