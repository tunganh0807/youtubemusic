package com.anhtung.youtubemusic.ui.custom

import android.content.Context
import android.util.AttributeSet
import com.anhtung.youtubemusic.R
import com.anhtung.youtubemusic.common.Constant
import com.anhtung.youtubemusic.common.MessageEvent
import com.tuanhav95.drag.DragView
import com.tuanhav95.drag.utils.inflate
import com.tuanhav95.drag.utils.reWidth
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.layout_top.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class DragSourceView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : DragView(context, attrs, defStyleAttr) {
    var mWidthWhenMax = 0
    var mWidthWhenMiddle = 0
    var mWidthWhenMin = 0

    init {
        getFrameFirst().addView(inflate(R.layout.layout_top))
        getFrameSecond().addView(inflate(R.layout.layout_bottom))
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        EventBus.getDefault().register(this)
    }

    override fun onDetachedFromWindow() {
        EventBus.getDefault().unregister(this)
        super.onDetachedFromWindow()
    }

    override fun initFrame() {
        mWidthWhenMax = width
        mWidthWhenMiddle = (width - mPercentWhenMiddle * mMarginEdgeWhenMin).toInt()
        mWidthWhenMin = mHeightWhenMinDefault * 17 / 9
        super.initFrame()
    }

    override fun refreshFrameFirst() {
        super.refreshFrameFirst()
        val width = if (mCurrentPercent < mPercentWhenMiddle) {
            EventBus.getDefault().post(MessageEvent(Constant.HIDE_NAVIGATION))
            (mWidthWhenMax - (mWidthWhenMax - mWidthWhenMiddle) * mCurrentPercent)
        } else {
            EventBus.getDefault().post(MessageEvent(Constant.SHOW_NAVIGATION))
//            seekBar.showBufferingProgress
            (mWidthWhenMiddle - (mWidthWhenMiddle - mWidthWhenMin) * (mCurrentPercent - mPercentWhenMiddle) / (1 - mPercentWhenMiddle))
        }

        frameTop.reWidth(width.toInt())
        ivClose.setOnClickListener(OnClickListener {
            dragView.close()
            EventBus.getDefault().post(MessageEvent(Constant.PAUSE_VIDEO))
        })

        iv_play_video.setOnClickListener {
            if (iv_play_video.drawable.level == 0) {
                iv_play_video.setImageLevel(1)
                EventBus.getDefault().post(MessageEvent(Constant.PAUSE_VIDEO))
            } else if (iv_play_video.drawable.level == 1) {
                iv_play_video.setImageLevel(0)
                EventBus.getDefault().post(MessageEvent(Constant.PLAY_VIDEO))
            }
        }


    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onShowMyDesign(event: MessageEvent) {
        when (event.typeEvent) {
            Constant.TYPE_TITLE ->
                tvTitle.text = event.stringValue
            Constant.TYPE_TITLE_CHANNEL ->
                tvChannel.text = event.stringValue
        }
    }
}