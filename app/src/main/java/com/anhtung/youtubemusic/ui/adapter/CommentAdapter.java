package com.anhtung.youtubemusic.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.data.model.comment.ItemComment;
import com.anhtung.youtubemusic.databinding.ItemCommentBinding;
import com.anhtung.youtubemusic.utils.ConvertCount;
import com.bumptech.glide.Glide;

import java.util.List;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentHolder> {
    private List<ItemComment> listItem;
    private final Context mContext;

    public CommentAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setListItem(List<ItemComment> listItem) {
        this.listItem = listItem;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CommentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCommentBinding viewBinding = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_comment, parent, false);
        return new CommentHolder(viewBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentHolder holder, int position) {
        holder.bind(listItem.get(position));
    }

    @Override
    public int getItemCount() {
        if (listItem != null) {
            return listItem.size();
        }
        return 0;
    }

    public static class CommentHolder extends RecyclerView.ViewHolder {
        ItemCommentBinding viewBinding;

        public CommentHolder(@NonNull ItemCommentBinding v) {
            super(v.getRoot());
            viewBinding = v;
        }

        public void bind(ItemComment item) {
            Glide.with(viewBinding.ivAuthorImage).load(item.getSnippet().getTopLevelComment()
                    .getSnippet().getAuthorProfileImageUrl()).into(viewBinding.ivAuthorImage);
            viewBinding.tvAuthorName.setText(item.getSnippet().getTopLevelComment().getSnippet()
                    .getAuthorDisplayName());
            viewBinding.tvTime.setText(ConvertCount.convertTime(viewBinding.getRoot().getContext(),
                    item.getSnippet().getTopLevelComment().getSnippet().getPublishedAt()));
            viewBinding.tvDisplay.setText(item.getSnippet().getTopLevelComment()
                    .getSnippet().getTextOriginal()
            );

        }
    }
}
