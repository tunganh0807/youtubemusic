package com.anhtung.youtubemusic.ui.main.search_pager;

import androidx.lifecycle.MutableLiveData;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.common.Constant;
import com.anhtung.youtubemusic.data.local.entities.Suggestion;
import com.anhtung.youtubemusic.data.model.search.Search;
import com.anhtung.youtubemusic.data.respository.SuggestionRepository;
import com.anhtung.youtubemusic.data.respository.YoutubeRepository;
import com.anhtung.youtubemusic.ui.base.BaseViewModel;

import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

@HiltViewModel
public class SearchPagerViewModel extends BaseViewModel {
    public final MutableLiveData<Search> newSearch = new MutableLiveData<>();
    public final MutableLiveData<Search> searchVideo = new MutableLiveData<>();
    public final MutableLiveData<List<Suggestion>> suggestionMutableLiveData = new MutableLiveData<>();
    private final YoutubeRepository youtubeRepository;
    MutableLiveData<Integer> sms = new MutableLiveData<>();

    @Inject
    public SearchPagerViewModel(YoutubeRepository youtubeRepository) {
        this.youtubeRepository = youtubeRepository;
    }

    public void getVideoSearch(String title) {
        youtubeRepository.getHandlerVideoSearch(Constant.MAX_RESULT, title, Constant.VIDEO, Constant.API_KEY)
                .subscribe(new SingleObserver<Search>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(@NonNull Search search) {
                        newSearch.postValue(search);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        sms.postValue(R.string.key_error);
                    }
                });
    }

    public void getSearchVideo(String title) {
        youtubeRepository.getHandlerVideoSearch(Constant.MAX_RESULT, title, Constant.VIDEO, Constant.API_KEY)
                .subscribe(new SingleObserver<Search>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        compositeDisposable.add(d);
                    }

                    @Override
                    public void onSuccess(@NonNull Search search) {
                        searchVideo.postValue(search);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        sms.postValue(R.string.key_error);
                    }
                });
    }

    public void addSuggestion(Suggestion suggestion){
        SuggestionRepository.getInstance().addSuggestion(suggestion);
    }


}
