package com.anhtung.youtubemusic.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.data.model.search.ItemSearch;
import com.anhtung.youtubemusic.data.model.video.Item;
import com.anhtung.youtubemusic.databinding.ItemNewSearchBinding;

import java.util.List;

public class TitleSearchAdapter extends RecyclerView.Adapter<TitleSearchAdapter.TitleSearchHolder> {
    private final Context mContext;
    private final MutableLiveData<Item> itemResult = new MutableLiveData<>();
    private List<ItemSearch> listItem;

    public TitleSearchAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setListItem(List<ItemSearch> listItem) {
        this.listItem = listItem;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TitleSearchHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemNewSearchBinding viewBinding = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_new_search, parent, false);
        return new TitleSearchHolder(viewBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull TitleSearchHolder holder, int position) {
        holder.bind(listItem.get(position));
    }

    @Override
    public int getItemCount() {
        if (listItem != null) {
            return listItem.size();
        }
        return 0;
    }

    private void showDetailVideo(Object tag) {
        itemResult.setValue((Item) tag);
    }

    public MutableLiveData<Item> getItemResult() {
        return itemResult;
    }

    public class TitleSearchHolder extends RecyclerView.ViewHolder {
        ItemNewSearchBinding viewBinding;

        public TitleSearchHolder(@NonNull ItemNewSearchBinding v) {
            super(v.getRoot());
            viewBinding = v;
        }

        public void bind(ItemSearch item) {
            viewBinding.tvTitle.setTag(item.getVideoItem().getItems().get(0));
            viewBinding.tvTitle.setText(item.getSnippet().getTitle());

            viewBinding.lnMain.setOnClickListener(v -> {
                if (!viewBinding.tvTitle.getTag().equals("") && viewBinding.tvTitle.getTag() != null) {
                    showDetailVideo(viewBinding.tvTitle.getTag());
                }else{
                    Toast.makeText(v.getContext(), R.string.data_is_loading, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}

