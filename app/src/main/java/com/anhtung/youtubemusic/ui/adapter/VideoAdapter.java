package com.anhtung.youtubemusic.ui.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.data.model.video.Item;
import com.anhtung.youtubemusic.databinding.ItemHomePagerBinding;
import com.anhtung.youtubemusic.utils.ConvertCount;
import com.bumptech.glide.Glide;

import java.time.Duration;
import java.util.List;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoHolder> {
    private final Context mContext;
    private final MutableLiveData<Item> itemResult = new MutableLiveData<>();
    private List<Item> listItem;

    public VideoAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setListItem(List<Item> listItem) {
        this.listItem = listItem;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VideoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemHomePagerBinding viewBinding = DataBindingUtil.inflate(LayoutInflater
                .from(parent.getContext()), R.layout.item_home_pager, parent, false);
        return new VideoHolder(viewBinding);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull VideoHolder holder, int position) {
        holder.bind(listItem.get(position));
    }

    @Override
    public int getItemCount() {
        if (listItem != null) {
            return listItem.size();
        }
        return 0;
    }

    private void showDetailVideo(Object tag) {
        itemResult.setValue((Item) tag);
    }

    public MutableLiveData<Item> getItemResult() {
        return itemResult;
    }

    public class VideoHolder extends RecyclerView.ViewHolder {
        ItemHomePagerBinding viewBinding;

        public VideoHolder(@NonNull ItemHomePagerBinding v) {
            super(v.getRoot());
            viewBinding = v;
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        public void bind(Item item) {
            if (item.getSnippet().getThumbnails().getHigh().getUrl() == null) {
                Glide.with(viewBinding.ivThumbnails).load(R.drawable.ic_launcher_background)
                        .override(375, 216).into(viewBinding.ivThumbnails);
            } else {
                Glide.with(viewBinding.ivThumbnails).load(item.getSnippet().getThumbnails()
                        .getHigh().getUrl()).override(375, 216).into(viewBinding.ivThumbnails);
            }

            Glide.with(viewBinding.ivChannelIcon).load(item.getChannel().getItems()
                    .get(0).getSnippet().getThumbnails().getHigh().getUrl())
                    .into(viewBinding.ivChannelIcon);

            viewBinding.tvTitle.setTag(item);
            viewBinding.tvTitle.setText(item.getSnippet().getTitle());
            viewBinding.tvChannelTitle.setText(item.getSnippet().getChannelTitle());
            viewBinding.tvViewCount.setText(ConvertCount.convertViewCount(
                    viewBinding.getRoot().getContext(), item.getStatistics().getViewCount())
            );
            viewBinding.tvTime.setText(ConvertCount.convertTime(
                    viewBinding.getRoot().getContext(), item.getSnippet().getPublishedAt())
            );
            viewBinding.tvDuration.setText(ConvertCount.convertDuration(Duration.parse(
                    item.getContentDetails().getDuration()).getSeconds())
            );

            viewBinding.lnVideo.setOnClickListener(v ->
                    showDetailVideo(viewBinding.tvTitle.getTag())
            );
        }
    }
}

