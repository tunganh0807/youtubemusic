package com.anhtung.youtubemusic.ui.main.home_pager;

import androidx.lifecycle.MutableLiveData;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.common.Constant;
import com.anhtung.youtubemusic.data.model.search.Search;
import com.anhtung.youtubemusic.data.respository.YoutubeRepository;
import com.anhtung.youtubemusic.ui.base.BaseViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

@HiltViewModel
public class HomePagerViewModel extends BaseViewModel {
    private final YoutubeRepository youtubeRepository;
    MutableLiveData<Search> itemVideoSearch = new MutableLiveData<>();
    MutableLiveData<Integer> sms = new MutableLiveData<>();

    @Inject
    public HomePagerViewModel(YoutubeRepository youtubeRepository) {
        this.youtubeRepository = youtubeRepository;
    }

    public void getVideoSearch() {
        youtubeRepository.fixSearchApi(Constant.MAX_RESULT, "Sơn Tùng M-TP",
                Constant.VIDEO, Constant.API_KEY).subscribe(new SingleObserver<Search>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
                compositeDisposable.add(d);
            }

            // getValue là đẩy dữ liệu lên từ A -> B
            // postValue là đẩy dữ liệu từ A -> B nhưng có thể chạy dưới nền
            @Override
            public void onSuccess(@NonNull Search search) {
                itemVideoSearch.postValue(search);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                sms.postValue(R.string.key_error);
            }
        });
    }
}


