package com.anhtung.youtubemusic.ui.dialog;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.common.Constant;
import com.anhtung.youtubemusic.databinding.ViewLanguageBinding;
import com.anhtung.youtubemusic.ui.base.BaseBindingDialogFragment;
import com.anhtung.youtubemusic.utils.LocaleUtils;

public class LanguageDialog extends BaseBindingDialogFragment<ViewLanguageBinding> {
    @Override
    public int getLayoutId() {
        return R.layout.view_language;
    }

    @Override
    public void onStart() {
        super.onStart();
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.9);
        getDialog().setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        if (LocaleUtils.codeLanguageCurrent.equals(Constant.LANGUAGE_VN)) {
            binding.rbVn.setChecked(true);
        } else {
            binding.rbEn.setChecked(true);
        }

        binding.rbVn.setOnClickListener(v -> {
            LocaleUtils.applyLocaleAndRestart(requireActivity(), Constant.LANGUAGE_VN);
        });
        binding.rbEn.setOnClickListener(v -> {
            LocaleUtils.applyLocaleAndRestart(requireActivity(), Constant.LANGUAGE_EN);
        });
        binding.btButton.setOnClickListener(v -> dismiss());
    }
}

