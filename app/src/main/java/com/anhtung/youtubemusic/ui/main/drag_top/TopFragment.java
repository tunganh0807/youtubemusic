package com.anhtung.youtubemusic.ui.main.drag_top;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.common.Constant;
import com.anhtung.youtubemusic.common.MessageEvent;
import com.anhtung.youtubemusic.data.local.SharedPreferenceHelper;
import com.anhtung.youtubemusic.data.model.video.Item;
import com.anhtung.youtubemusic.databinding.FragmentTopBinding;
import com.anhtung.youtubemusic.ui.base.BaseBindingFragment;
import com.anhtung.youtubemusic.ui.main.MainActivity;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerFullScreenListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class TopFragment extends BaseBindingFragment<FragmentTopBinding, TopViewModel> {
    private final YouTubePlayerFullScreenListener fullScreenListener = new YouTubePlayerFullScreenListener() {
        @Override
        public void onYouTubePlayerEnterFullScreen() {
            EventBus.getDefault().post(new MessageEvent(Constant.ENTER_FULL_SCREEN));
            requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        @Override
        public void onYouTubePlayerExitFullScreen() {
            EventBus.getDefault().post(new MessageEvent(Constant.EXIT_FULL_SCREEN));
            requireActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        }
    };
    private YouTubePlayer youTubePlayer;
    private final AbstractYouTubePlayerListener listener = new AbstractYouTubePlayerListener() {
        @Override
        public void onReady(@NonNull YouTubePlayer youTubePlayer) {
            TopFragment.this.youTubePlayer = youTubePlayer;
        }
    };
    private List<Item> list = new ArrayList<>();

    public TopFragment() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        // Đăng kí event bus
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDetach() {
        EventBus.getDefault().unregister(this);
        super.onDetach();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShowMyDesign(MessageEvent event) {
        switch (event.getTypeEvent()) {
            case Constant.PLAY_VIDEO:
                if (youTubePlayer != null) {
                    youTubePlayer.play();
                }
                break;
            case Constant.PAUSE_VIDEO:
                if (youTubePlayer != null) {
                    youTubePlayer.pause();
                }
                break;
            case Constant.PREVIOUS_VIDEO:
                list = event.getListItem();
                break;
        }
    }

    @Override
    protected Class<TopViewModel> getViewModel() {
        return TopViewModel.class;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_top;
    }


    /*Xử lý video và các tác vụ*/
    @SuppressLint("UseCompatLoadingForDrawables")
    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        binding.youTubePlayerView.addYouTubePlayerListener(listener);
        EventBus.getDefault().post(new MessageEvent(Constant.SEEK_BAR));
        binding.seekBar.setThumb(requireContext().getDrawable(R.drawable.ic_seekbar));
    }

    //Xử lí chạy nền
    @Override
    public void onPause() {
        SharedPreferenceHelper sharedPreferenceHelper = new SharedPreferenceHelper(requireActivity(), Constant.PREF_SETTING_LANGUAGE);
        if (sharedPreferenceHelper.getBoolean(Constant.ON_PLAY_BACKGROUND, false)) {
            binding.youTubePlayerView.enableBackgroundPlayback(true);
            super.onPause();

            ((MainActivity) requireActivity()).showNotification();
        } else if (sharedPreferenceHelper.getBoolean(Constant.OFF_PLAY_BACKGROUND, false)) {
            binding.youTubePlayerView.enableBackgroundPlayback(false);
            super.onPause();

            ((MainActivity) requireActivity()).closeNotification();
        } else {
            super.onPause();
        }
    }

    @Override
    protected void onPermissionGranted() {

    }

    @SuppressLint("UseCompatLoadingForDrawables")
    public void initYoutubeScreen(String idVideo) {
        if (youTubePlayer != null && idVideo != null && !idVideo.isEmpty()) {
            // v:... là thời gian chờ cho video chạy khi dragview vừa hiện lên
            youTubePlayer.loadVideo(idVideo, 0f);
            youTubePlayer.addListener(binding.seekBar);

            EventBus.getDefault().post(new MessageEvent(Constant.LEVEL_PLAY));
        } else {
            binding.youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
                @Override
                public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                    super.onReady(youTubePlayer);
                    assert idVideo != null;
                    youTubePlayer.cueVideo(idVideo, 0f);
                }
            });
            EventBus.getDefault().post(new MessageEvent(Constant.LEVEL_PLAY));
        }

        binding.seekBar.setYoutubePlayerSeekBarListener(time -> youTubePlayer.seekTo(time));
        if (list.size() >= 2) {
            binding.youTubePlayerView.getPlayerUiController().setCustomAction1(
                    getResources().getDrawable(R.drawable.ic_previos_on), v -> previousVideo()
            );
        } else {
            binding.youTubePlayerView.getPlayerUiController().setCustomAction1(
                    getResources().getDrawable(R.drawable.ic_previous_off), v -> {
                    }
            );
        }

        binding.youTubePlayerView.getPlayerUiController().setCustomAction2(
                getResources().getDrawable(R.drawable.ic_next_off), v ->
                        EventBus.getDefault().post(new MessageEvent(Constant.NEXT_VIDEO))
        );
        binding.youTubePlayerView.getPlayerUiController().showCustomAction1(true);
        binding.youTubePlayerView.getPlayerUiController().showCustomAction2(true);
        binding.youTubePlayerView.getPlayerUiController().showYouTubeButton(false);
        binding.youTubePlayerView.addFullScreenListener(fullScreenListener);
    }

    private void previousVideo() {
        if (list.size() >= 2) {
            Item itemRemove = list.get(list.size() - 1);
            list.remove(itemRemove);
            EventBus.getDefault().post(new MessageEvent(itemRemove));
        }
    }

    public void hideIconVideo() {
        initVisible(false, View.VISIBLE);
    }

    public void showIconVideo() {
        initVisible(true, View.GONE);
    }

    private void initVisible(boolean check, int gone) {
        binding.youTubePlayerView.getPlayerUiController().showPlayPauseButton(check);
        binding.youTubePlayerView.getPlayerUiController().showSeekBar(check);
        binding.youTubePlayerView.getPlayerUiController().showFullscreenButton(check);
        binding.youTubePlayerView.getPlayerUiController().showVideoTitle(check);
        binding.youTubePlayerView.getPlayerUiController().showCustomAction1(check);
        binding.youTubePlayerView.getPlayerUiController().showCustomAction2(check);
        binding.seekBar.setVisibility(gone);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.youTubePlayerView.removeYouTubePlayerListener(listener);
        binding.youTubePlayerView.removeFullScreenListener(fullScreenListener);
        binding.youTubePlayerView.release();
    }
}

