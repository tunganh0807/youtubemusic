package com.anhtung.youtubemusic.ui.dialog;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.databinding.ViewProgressBinding;
import com.anhtung.youtubemusic.ui.base.BaseBindingDialogFragment;

public class ProgressDialog extends BaseBindingDialogFragment<ViewProgressBinding> {
    @Override
    public int getLayoutId() {
        return R.layout.view_progress;
    }

    @Override
    public void onStart() {
        super.onStart();
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.8);
        getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);

        getDialog().setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);
    }

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {

    }
}
