package com.anhtung.youtubemusic.ui.dialog;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import com.anhtung.youtubemusic.R;
import com.anhtung.youtubemusic.common.Constant;
import com.anhtung.youtubemusic.data.local.SharedPreferenceHelper;
import com.anhtung.youtubemusic.databinding.ViewPlayBackgroundBinding;
import com.anhtung.youtubemusic.ui.base.BaseBindingDialogFragment;

public class PlayBackgroundDialog extends BaseBindingDialogFragment<ViewPlayBackgroundBinding> {
    private int type;
    private SharedPreferenceHelper sharedPreferenceHelper;

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public int getLayoutId() {
        return R.layout.view_play_background;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().getWindow().setLayout((int) (getResources().getDisplayMetrics().widthPixels * 0.9),
                ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    protected void onCreatedView(View view, Bundle savedInstanceState) {
        sharedPreferenceHelper = new SharedPreferenceHelper(requireContext(),
                Constant.PREF_SETTING_LANGUAGE);
        if (type == Constant.TYPE_HOME) {
            initView(requireContext().getString(R.string.txt_play_background)
                    , sharedPreferenceHelper.getBoolean(Constant.ON_PLAY_BACKGROUND, false), Constant.ON_PLAY_BACKGROUND, Constant.OFF_PLAY_BACKGROUND);
        }
    }

    private void initView(String text, boolean check, String idOn, String idOff) {
        binding.txtTitle.setText(text);
        if (check)
            binding.rbOn.setChecked(true);
        else {
            binding.rbOff.setChecked(true);
        }
        binding.rbOn.setOnCheckedChangeListener((buttonView, isChecked) -> {
            sharedPreferenceHelper.storeBoolean(idOn, isChecked);
        });
        binding.rbOff.setOnCheckedChangeListener((buttonView, isChecked) -> {
            sharedPreferenceHelper.storeBoolean(idOff, isChecked);
        });
        binding.btButton.setOnClickListener(v -> dismiss());
    }
}

