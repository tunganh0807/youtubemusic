package com.anhtung.youtubemusic.ui.main.drag_top;

import com.anhtung.youtubemusic.ui.base.BaseViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class TopViewModel extends BaseViewModel {
    @Inject
    public TopViewModel() {
    }
}